import os

from banyan3.banyan3 import Banyan3


FILE_DIR = os.path.dirname(os.path.abspath(__file__))


def test_banyan_wiki():
    """
    Test the output of banyan_wiki
    """
    file_path = os.path.join(FILE_DIR, "..", "examples", "wiki_banyan.json")
    b3 = Banyan3(file_path)

    html_file = os.path.join(FILE_DIR, "..", "html",\
     "https _en.wikipedia.org_wiki_Banyan.html")

    with open(html_file, encoding="utf8") as f:
        html = f.read()


    results = b3.extract_data(html)

    output = {
        'sub_title_0': 'Characteristics',
        'sub_title_1': 'Etymology',
        'sub_title_2': 'Classification',
        'sub_title_3': 'In horticulture',
        'sub_title_4': 'In culture',
        'sub_title_5': 'Religion and mythology',
        'sub_title_6': 'Notable specimens',
        'sub_title_7': 'Other',
        'sub_title_8': 'See also',
        'sub_title_9': 'References',
        'sub_title_10': 'External links'
        }

    assert results == output



if __name__=="__main__":
    print(FILE_DIR)

