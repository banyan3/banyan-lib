# Vision
Our vision is to make a tool which can help scrape data from an HTML file by writing a file which describes what data needs to be scraped. 

# Introduction
The aim of the project is to create a tool to make it easier to scrape data from HTML with the least amount of coding. 



# Examples

1. https://www.staples.com/Paper-Punches/cat_CL160904?icid=OFFICEBASICSCAT:CAROUSEL1:POPCATEGORIES12:PAPERPUNCHES
1. File name : `examples\staples_tree.json`




# Simple Example
1. https://en.wikipedia.org/wiki/Banyan
1. File name : `examples\wiki_banyan.json`


From the above wiki page we have to extract all titles in the article.