import json
from bs4 import BeautifulSoup



def select_group(tag_list, node):
    """
    Select the container of data from the list of tags

    Parameters
    -------------
    tag_list : list of BeautifulSoup tags

    node : dict
    """
    result_dict = dict()
    for tag in tag_list:
        # print("==" * 20)
        # print(tag)
        # print("==" * 30)
        node_type = node.get("type")
        attribute_dict = attribute_string(node)
        node_tag = node.get("tag")
        if node_type == "container":
           

            # print(type(tag))
            all_tags_list = tag.find_all(node_tag, attrs=attribute_dict)

            child_node = node.get("node")
            result = select_group(all_tags_list, child_node)
            result_dict.update(result)
        elif node_type == "data":
            # select the tag
            all_tags_list = tag.find_all(node_tag, attrs=attribute_dict)

            # print(len(all_tags_list))
            for idx, sub_tag in enumerate(all_tags_list):
                for attrb in node.get("attributes"):
                    if attrb == "text":
                        # print(tag)
                        result_dict[node["attributes"][attrb] + "_" + str(idx)] = sub_tag.text.strip()
                    else:
                        result_dict[node["attributes"][attrb] + "_" + str(idx)] = sub_tag.get(attrb)
            return result_dict
    return result_dict








def attribute_string(node):
    """
    Create a dictionary type attributes list which
    can be used in `Beautifulsoup` attribute
    constraint.

    Parameters
    ------------
    node : dict
    """
    skip_tags = {"tag", "type", "node", "attributes"}

    attribute_dict = dict()

    for key in node:
        if key in skip_tags:
            continue
        attribute_dict[key] = node.get(key)

    return attribute_dict
        



class Banyan3(object):
    """
    Parameters
    ---------------
    banyan3_file : str
        file path to the banyan3 configuration json file.
    """
    def __init__(self, banyan3_file):
        self.file = banyan3_file
        self.js = json.load(open(self.file))


    def __validate_json(self):
        # TODO: raise the todo file
        raise NotImplementedError


    def extract_data(self, html):
        """
        Extract data from given HTML.

        Parameters
        ------------
        html : str
            html markup of the page from which information
            has to be extracted.
        """
        soup = BeautifulSoup(html, "html.parser")

        for node in self.js:
            # send in the HTML tag, i.e. the entire webpage
            result = select_group(soup.find_all('html'), node)

            print("++" * 20)
            print(result)
            print("++" * 30)
            return result


            