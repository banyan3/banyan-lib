import os
import requests
from banyan3.banyan3 import Banyan3


FILE_DIR = os.path.dirname(os.path.abspath(__file__))



def main():
    file_path = os.path.join(FILE_DIR, "examples", "wiki_banyan.json")

    b3 = Banyan3(file_path)

    url = "https://en.wikipedia.org/wiki/Banyan"

    r = requests.get(url)

    # print(r.text)

    b3.extract_data(r.text)
    



if __name__ == "__main__":
    main()