# Introduction

In this examples we will show how you can use 

```python
>>> import time
>>> import os


def helloworld(time):
    """
    This
    """
``` 

# `JSON` keys supported

The following keys are supported in the `json` configuration file.

1. `tag`
1. `class`
1. `type`
1. `node`


# Banyan3 configuration file

A `banyan3` configuration file is a `json` file whose top
element is an array of nodes. Each `node` can be one of two
types - _data_ or _container_. A _data_ type node defines
what data needs to be extracted from the current
_container_ tag. A _container_ type node defines the
sub_container of the present container. For this tutorial
we will assume that the HTML is as shown below



## Example Data

```html
<html>
    <head>
    </head>
    <body>
     <ul class="list1">
        <li> list_1_item_1 </li>
        <li> list_1_item_2 </li>
        <li> list_1_item_3 </li>
     </ul>

     <ul class="list2">
        <li> list_2_item_1 </li>
        <li> list_2_item_2 </li>
        <li> list_2_item_3 </li>
     </ul>
    
    </body>
</html>
```

We will create a `banyan3` configuration file such that we
extract the list items `li` tag from `ul` with 
`"class=list2"`.

1. Root is an array of nodes hence we will start with an empty JSON array.
    ```js
    [

    ]
    ```
1. Since, the `li` tag we are interested in are in `ul` tag
with class `list2`. We will create a _container_ node first, for now we will keep the child node empty.
    ```js
    [
        "tag" : "ul",
        "class" : "list2",
        "type" : "container",
        "node": [
        ]
    ]
    ```
1. Once in the _container_ node we now have to select text (or _data_) from the `li` elements. For this the node will be written as
    ```js
    [
        "tag" : "li",
        "class" : "",
        "type" : "data",
        "attributes" : {
            "text": "l2_item"
        }
    ]
    ```
    Notice that the `class` is an empty string because there is no associated class.






